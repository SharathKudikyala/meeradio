package com.app.weaver.telanganafm.utils;

import com.app.weaver.telanganafm.utils.Constants;

import java.io.File;

public class FileHelper {

    public static File getAudioFolder() {
        File file = null;
        try {
            file = new File(Constants.DIR_PATH);
            if (!file.exists())
                file.mkdir();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

}
