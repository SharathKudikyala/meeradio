package com.app.weaver.telanganafm.utils;

import android.os.Environment;

import com.app.weaver.telanganafm.MyApplication;
import com.app.weaver.telanganafm.R;

public class Constants {
    public static String RADIO_URL = "http://s2.voscast.com:12682/;/;stream.mp3";
    public static String DIR_PATH = String.format("%s/%s/", Environment.getExternalStorageDirectory(), MyApplication.getInstance().getApplicationContext().getString(R.string.app_name));
}
