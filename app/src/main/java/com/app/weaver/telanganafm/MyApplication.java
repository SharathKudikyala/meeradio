package com.app.weaver.telanganafm;

import androidx.multidex.MultiDexApplication;

import com.app.weaver.telanganafm.utils.NotificationHelper;
import com.google.android.gms.ads.MobileAds;

public class MyApplication extends MultiDexApplication {

    private static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        new NotificationHelper(instance).createNotificationChannel();
    }

    public static MyApplication getInstance() {
        return instance;
    }

}
