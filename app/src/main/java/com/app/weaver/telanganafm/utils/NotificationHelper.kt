package com.app.weaver.telanganafm.utils


import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.app.weaver.telanganafm.MainActivity
import com.app.weaver.telanganafm.R
import com.google.firebase.messaging.RemoteMessage
import java.net.HttpURLConnection
import java.net.URL

const val CHANNEL_ANNOUNCEMENT = "ANNOUNCEMENTS"
const val CHANNEL_ANNOUNCEMENT_ID = "CH1_ANNOUNCEMENT"

class NotificationHelper(private val context: Context) {

    private var builder: NotificationCompat.Builder? = null


    fun createNotification(message: RemoteMessage) {
        try {

            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)

            val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            builder = NotificationCompat.Builder(context, CHANNEL_ANNOUNCEMENT_ID)
            builder?.setSmallIcon(R.drawable.ic_notification)
            builder?.setContentTitle(message.notification?.title ?: "")
            builder?.setContentText(message.notification?.body ?: "")
            builder?.setAutoCancel(false)
            builder?.color = Color.parseColor("#FBC02D")
            builder?.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            builder?.setContentIntent(pendingIntent)
            //builder?.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
            if (message.notification?.imageUrl?.toString()?.isEmpty() == false) {
                showBigPictureNotification(message, builder!!)
            } else {
                builder?.setStyle(NotificationCompat.BigTextStyle()
                        .setBigContentTitle(message.notification?.title)
                        .bigText(message.notification?.body)
                        .setSummaryText(message.notification?.body))

                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(Global.createID(), builder?.build()!!)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(CHANNEL_ANNOUNCEMENT_ID, CHANNEL_ANNOUNCEMENT, NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.description = "This is for general announcements"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.YELLOW

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun showBigPictureNotification(message: RemoteMessage, builder: NotificationCompat.Builder) {
        try {
            val url = URL(message.notification?.imageUrl?.toString())
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            val bitmap: Bitmap = BitmapFactory.decodeStream(input)

            builder.setLargeIcon(bitmap)
            builder.setStyle(NotificationCompat.BigPictureStyle()
                    .bigPicture(bitmap)
                    .bigLargeIcon(null)
                    .setBigContentTitle(message.notification?.title ?: "")
                    .setSummaryText(message.notification?.body ?: ""))

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(Global.createID(), builder.build())
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}

