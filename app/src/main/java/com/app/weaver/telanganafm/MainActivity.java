package com.app.weaver.telanganafm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

import com.app.weaver.telanganafm.utils.Constants;
import com.app.weaver.telanganafm.utils.FileHelper;
import com.app.weaver.telanganafm.utils.Global;
import com.app.weaver.telanganafm.widgets.FallNoteAnim;
import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Release note
 * Key : fm
 * Password : password
 */

public class MainActivity extends AppCompatActivity {
    AudioManager audioManager;
    FloatingActionButton fabPlayOrStop;
    ProgressDialog progressDialog;
    FallNoteAnim fallNoteAnim;
    SimpleExoPlayer player;
    Button btnRecord;
    AppCompatSeekBar seekBar;
    TextView tvMarqueeContent;
    AdRequest adRequest;
    ImageSlider imageSlider;
    OutputStream outputStream;
    AsyncTask<Void, Void, Void> recordingTask;
    private final String TAG = this.getClass().getSimpleName();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    ArrayList<SlideModel> imageList = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!Global.isOnline())
            showAlert();
        initialize();
        initializeEvents();
        getAds();

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageSlider.setVisibility(imageSlider.getVisibility() == View.VISIBLE ? View.INVISIBLE : View.VISIBLE);
                    }
                });
            }
        }, 2000, 20000);
    }

    private void initialize() {

        //region Inflate Views
        fabPlayOrStop = findViewById(R.id.fabPlayOrStop);
        fallNoteAnim = findViewById(R.id.vusik);
        btnRecord = findViewById(R.id.btnRecord);
        tvMarqueeContent = findViewById(R.id.tvMarqueeContent);
        seekBar = findViewById(R.id.seekBar);
        imageSlider = findViewById(R.id.imageSlider);
        //endregion

        //region Prepare Progress Dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        //endregion

        //region Initialize AudioManager
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        seekBar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        seekBar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
        //endregion

        //region Start scrolling content
        tvMarqueeContent.setSelected(true);
        //endregion

    }

    private void initializeEvents() {

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isStoragePermissionEnabled())
                    return;

                if (fabPlayOrStop.getTag().toString().equals("play"))
                    return;

                recordOrStop("REC".equalsIgnoreCase(v.getTag().toString()));
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (audioManager != null)
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    private void showAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setMessage("Please Check Internet Connection and Try Again..!!");
        alertDialog.setCancelable(false);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
    }

    private boolean isStoragePermissionEnabled() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PermissionChecker.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        }
        return false;
    }

    private void initializeCustomAds() {

        //region Set ImageSlider
        imageSlider.setImageList(imageList, true);
        imageSlider.animate();
        //endregion

    }

    public void playOrStop(View view) {
        try {
            if (fabPlayOrStop.getTag().toString().equals("play")) {
                progressDialog.show();

                //region Initialize Exo Player
                player = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), new DefaultTrackSelector());
                DefaultDataSourceFactory defaultDataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "Telangana FM"));
                ExtractorMediaSource extractorMediaSource = new ExtractorMediaSource.Factory(defaultDataSourceFactory).createMediaSource(Uri.parse(Constants.RADIO_URL));
                player.prepare(extractorMediaSource);
                //endregion

                fabPlayOrStop.setTag("stop");
                fabPlayOrStop.setImageResource(R.drawable.ic_round_stop);
                player.setPlayWhenReady(true);
                //player.getPlaybackState();
                fallNoteAnim.start();
                fallNoteAnim.startNotesFall();
                progressDialog.dismiss();
            } else {
                fabPlayOrStop.setTag("play");
                fabPlayOrStop.setImageResource(R.drawable.ic_round_play);
                player.setPlayWhenReady(false);
                //player.getPlaybackState();
                if (fallNoteAnim != null)
                    fallNoteAnim.stopNotesFall();

                recordOrStop(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void volumeUp(View view) {
        audioManager.adjustVolume(1, 4);
    }

    public void volumeDown(View view) {
        audioManager.adjustVolume(-1, 4);
    }

    public void recordOrStop(boolean flag) {
        if (flag) {
            //region Start recording
            btnRecord.setTag("STOP");
            btnRecord.setText(R.string.stop_rec);
            Snackbar.make(btnRecord, "Recording started", Snackbar.LENGTH_LONG).show();

            recordingTask = new MyTask().execute();

            //endregion
        } else {
            //region Stop saving
            btnRecord.setTag("REC");
            btnRecord.setText(R.string.rec);

            try {
                if (outputStream != null) {
                    outputStream.close();
                    if (recordingTask != null && AsyncTask.Status.FINISHED != recordingTask.getStatus()) {
                        recordingTask.cancel(true);
                        Snackbar.make(btnRecord, "Recorded audio saved successfully", Snackbar.LENGTH_LONG).show();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //endregion
        }
    }

    public void sendSMS(View view) {
        String formattedNumber = PhoneNumberUtils.formatNumber(getString(R.string.contact_number));
        try {

            Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=" + formattedNumber + "&text=Hello, Telangana FM!\n");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);

        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Please install WhatsApp, then try again.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Error : \n" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void openPrivacyPolicy(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.telanganafm.com/privacy_policy.html"));
        startActivity(Intent.createChooser(intent, "Open with"));
    }

    public void makeCall(View view) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.contact_number))));
    }

    private void getAds() {
        StorageReference reference = storage.getReference();
        reference.getStorage().getReference().child("Ads").listAll().addOnSuccessListener(
                new OnSuccessListener<ListResult>() {
                    @Override
                    public void onSuccess(ListResult listResult) {
                        final int size = listResult.getItems().size();
                        final int[] count = {0};
                        for (StorageReference item : listResult.getItems()) {
                            item.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    count[0]++;
                                    imageList.add(new SlideModel(uri.toString(), true));
                                    if (count[0] == size) {
                                        initializeCustomAds();
                                    }
                                }
                            });
                        }
                    }
                }
        ).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("Error", e.getMessage());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (player != null)
            player.release();
    }

    class MyTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                //region Open URL Connection
                URL url = new URL(Constants.RADIO_URL);
                InputStream inputStream = url.openStream();
                //endregion

                File folder = FileHelper.getAudioFolder();
                if (folder != null && folder.exists()) {
                    //region Clear stream
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    //endregion

                    //region Write stream of data to file
                    String FILE_PATH = String.format("%s%s.mp3/", Constants.DIR_PATH, Global.getCurrentDateTime());
                    outputStream = new FileOutputStream(FILE_PATH);
                    byte[] buffer = new byte[1];
                    int length;
                    while ((length = inputStream.read(buffer)) > 0) {
                        outputStream.write(buffer, 0, length);
                    }
                    //endregion

                    //region Inout stream data is empty close the file writing
                    outputStream.close();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnRecord.setTag("REC");
                            btnRecord.setText(R.string.rec);
                            Snackbar.make(btnRecord, "Recorded audio saved successfully", Snackbar.LENGTH_LONG).show();
                        }
                    });
                    //endregion
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recordOrStop(false);
                        }
                    });
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
