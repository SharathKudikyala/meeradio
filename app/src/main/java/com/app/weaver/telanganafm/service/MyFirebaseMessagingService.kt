package com.app.weaver.telanganafm.service

import android.util.Log
import com.app.weaver.telanganafm.utils.NotificationHelper
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        NotificationHelper(context = this).createNotification(message)
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.d("onNewToken", p0)
    }
}